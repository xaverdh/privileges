# privileges

## What it is

A haskell library to annotate IO actions with privilege information and allow control over the access rights used to execute various parts of the program.

## WARNING

This is NOT ready for use yet.

## Problems
There is no way to return a value from unprivileged code into privileged code.
This is a fundamental restriction in the sense that any such information flow poses a security risk.
Implementing this properly would require a lot of work, so it probably wont happen.

## How it works

TODO

