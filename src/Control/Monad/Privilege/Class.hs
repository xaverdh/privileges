module Control.Monad.Privilege.Class where

import Control.Monad.Privilege.Return
import System.Posix.Types (Fd)

class Monad m => MonadUnPrivileged m where
  unpriv :: Returnable r => IO a -> m r

class MonadUnPrivileged m => MonadUnPrivilegedWithFds m where
  unprivWithFds :: Returnable r => [Fd] -> IO a -> m r

class Monad m => MonadPrivileged m where
  priv :: IO a -> m a

class (MonadPrivileged m,MonadUnPrivileged m) => MonadPriv m

unprivWithFds' :: MonadUnPrivilegedWithFds m
  => [Fd] -> IO a -> m ()
unprivWithFds' = unprivWithFds

unpriv' :: MonadUnPrivileged m => IO a -> m ()
unpriv' = unpriv


