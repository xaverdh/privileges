{-# language ScopedTypeVariables, LambdaCase #-}
module Control.Monad.Privilege.Internal
  ( PrivPolicy(..)
  , UserPolicy(..)
  , GroupPolicy(..)
  , isRoot
  , withDroppedPrivsRoot
  , withDroppedPrivsNoRoot
  , withDroppedPrivs 
  , assertRootPrivs )
where

-- import Control.Monad.Privilege.CloseFds
import System.Unix.Spawn

import Data.Functor

import System.Posix.Process
import System.Posix.Types (UserID,GroupID,Fd)
import System.Posix.User
import System.Posix.IO

import System.Exit
import System.Environment (getEnv)
import System.IO.Error
import System.IO (hPutStrLn,stderr)

import Control.Monad.Extra
import Control.Exception

{- Priv dropping -}

data PrivPolicy a =
  SudoUser
  -- ^ Try reading from SUDO_* environment variables,
  --   if that fails exit the program with an error.
  | FallBack (IO a)
  -- ^ Try reading from SUDO_* environment variables,
  --   if that fails use provided id.
  | UseID (IO a)
  -- ^ Use provided id.

type UserPolicy = PrivPolicy UserID
type GroupPolicy = PrivPolicy GroupID


-- Throws an exception if variable not set
getTrueUserID :: IO UserID
getTrueUserID = read <$> getEnv "SUDO_UID"

-- Throws an exception if variable not set
getTrueGroupID :: IO GroupID
getTrueGroupID = read <$> getEnv "SUDO_GID"

safeGetTrueUserID :: IO UserID -> IO UserID
safeGetTrueUserID k = catchJust
  (\e -> if isDoesNotExistError e then Just () else Nothing)
  getTrueUserID (const k)

safeGetTrueGroupID :: IO GroupID -> IO GroupID
safeGetTrueGroupID k = catchJust
  (\e -> if isDoesNotExistError e then Just () else Nothing)
  getTrueGroupID (const k)


withDroppedPrivsRoot :: Bool -> UserPolicy -> GroupPolicy -> [Fd]
  -> IO a -> IO (Maybe ProcessStatus)
withDroppedPrivsRoot wait userPolicy groupPolicy inherit action = do
  -- obtain gid to assume
  gid <- case groupPolicy of
    SudoUser -> safeGetTrueGroupID
      $ dieWithError "Please use sudo to run this program."
    FallBack k -> safeGetTrueGroupID k
    UseID k -> k
  -- obtain uid to assume
  uid <- case userPolicy of
    SudoUser -> safeGetTrueUserID
     $ dieWithError "Please use sudo to run this program."
    FallBack k -> safeGetTrueUserID k
    UseID k -> k
  withDroppedPrivs' wait action $ cleanPolicy
    { fdPolicy = InheritFds inherit
    , uidPolicy = Just uid
    , gidPolicy = Just gid
    , groupsPolicy = Just [] }

withDroppedPrivsNoRoot :: Bool
  -> [Fd]
  -> IO a
  -> IO (Maybe ProcessStatus)
withDroppedPrivsNoRoot wait inherit action =
  withDroppedPrivs' wait action $ cleanPolicy
    { fdPolicy = InheritFds inherit }

withDroppedPrivs :: Bool -> UserPolicy -> GroupPolicy -> [Fd]
  -> IO a -> IO (Maybe ProcessStatus)
withDroppedPrivs wait userPolicy groupPolicy inherit action = do
  -- Are we root ?
  ifM isRoot
    ( withDroppedPrivsRoot wait userPolicy
        groupPolicy inherit action )
    ( withDroppedPrivsNoRoot wait inherit action )

withDroppedPrivs' :: Bool
  -> IO a
  -> SpawnPolicy
  -> IO (Maybe ProcessStatus)
withDroppedPrivs' wait action spawnPol = do
  -- Spawn the child process
  pid <- spawn spawnPol $ do
    action
    exitImmediately ExitSuccess
  -- Optionally wait for child process to exit
  if wait
    then getProcessStatus wait False pid
    else pure Nothing



{- Other -}

isRoot :: IO Bool
isRoot = getEffectiveUserID >>= pure . \case
  0 -> True
  _ -> False

assertRootPrivs :: IO () 
assertRootPrivs = unlessM isRoot $ dieWith
  (ExitFailure 2) "Insufficient privileges."


dieWith :: ExitCode -> String -> IO a
dieWith exCode err = do
  hPutStrLn stderr err
  exitWith exCode

dieWithError :: String -> IO a
dieWithError = dieWith (ExitFailure 1)

