{-# language GeneralizedNewtypeDeriving #-}
module Control.Monad.Privilege.Priv where

import Control.Monad.Privilege.Class
import Control.Monad.Privilege.Internal
import Control.Monad.Privilege.Return

import Control.Monad.IO.Class
import Control.Monad.Reader
import System.Posix.Types (Fd)
import Data.Functor


type Priv = PrivT IO

newtype PrivT m a = PrivT (ReaderT (UserPolicy,GroupPolicy) m a)
  deriving (Functor,Applicative,Monad)

instance MonadIO m => MonadPrivileged (PrivT m) where
  priv action = PrivT (liftIO action)

instance MonadIO m => MonadUnPrivileged (PrivT m) where
  unpriv = unprivWithFdsImpl []

instance MonadIO m => MonadUnPrivilegedWithFds (PrivT m) where
  unprivWithFds = unprivWithFdsImpl

instance MonadIO m => MonadPriv (PrivT m)

runPrivT :: MonadIO m => UserPolicy -> GroupPolicy -> PrivT m a -> m a
runPrivT userPolicy groupPolicy (PrivT action) =
  (liftIO assertRootPrivs >> action) `runReaderT` (userPolicy,groupPolicy)

runPriv :: UserPolicy -> GroupPolicy -> Priv a -> IO a
runPriv = runPrivT


unprivWithFdsImpl :: (Returnable r,MonadIO m)
  => [Fd] -> IO a -> PrivT m r
unprivWithFdsImpl inherit action =
  fmap ret . PrivT $ do
   userPolicy <- asks fst
   groupPolicy <- asks snd
   liftIO ( withDroppedPrivsRoot True
            userPolicy groupPolicy
            inherit action )


