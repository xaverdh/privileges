{-# language GeneralizedNewtypeDeriving #-}
module Control.Monad.Privilege.Privileged where

import Control.Monad.Privilege.Class
import Control.Monad.Privilege.Internal
import Control.Monad.Privilege.Return

import Control.Monad.IO.Class
import Data.Functor


type Privileged = PrivilegedT IO

newtype PrivilegedT m a = PrivilegedT (m a)
  deriving (Functor,Applicative,Monad)

instance MonadIO m => MonadPrivileged (PrivilegedT m) where
  priv action =
    PrivilegedT (liftIO action)

runPrivilegedT :: MonadIO m => PrivilegedT m a -> m a
runPrivilegedT (PrivilegedT action) =
  liftIO assertRootPrivs >> action

runPrivileged :: Privileged a -> IO a
runPrivileged = runPrivilegedT

