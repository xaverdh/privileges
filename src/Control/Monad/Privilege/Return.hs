{-# language LambdaCase, FlexibleInstances #-}
module Control.Monad.Privilege.Return where

import System.Posix.Process
import System.Exit

class Returnable r where
  ret :: Maybe ProcessStatus -> r

instance Returnable (Maybe ProcessStatus) where
  ret = id

instance Returnable ProcessStatus where
  ret = maybe (Exited ExitSuccess) id

instance Returnable ExitCode where
  ret = maybe ExitSuccess $ \case
    Exited exCode -> exCode
    Terminated _ _ -> ExitFailure (-1)
    Stopped _ -> ExitFailure (-1)

instance Returnable () where
  ret = const ()
