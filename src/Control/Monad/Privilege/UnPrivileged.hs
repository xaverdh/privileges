{-# language GeneralizedNewtypeDeriving #-}
module Control.Monad.Privilege.UnPrivileged where

import Control.Monad.Privilege.Class
import Control.Monad.Privilege.Internal
import Control.Monad.Privilege.Return

import Control.Monad.IO.Class
import System.Posix.Types (Fd)
import Data.Functor


type UnPrivileged = UnPrivilegedT IO

newtype UnPrivilegedT m a = UnPrivilegedT (m a)
  deriving (Functor,Applicative,Monad)

instance MonadIO m => MonadUnPrivileged (UnPrivilegedT m) where
  unpriv action =
    UnPrivilegedT (liftIO action)
    $> ret Nothing

runUnPrivilegedT :: Returnable r
  => UserPolicy -> GroupPolicy -> [Fd] -> UnPrivilegedT m a
  -> (m a -> IO b) -> IO r
runUnPrivilegedT userPolicy groupPolicy inherit (UnPrivilegedT action) run =
  ret <$> withDroppedPrivs True userPolicy groupPolicy inherit (run action)

runUnPrivileged :: Returnable r
  => UserPolicy -> GroupPolicy -> [Fd] -> UnPrivileged a -> IO r
runUnPrivileged userPolicy groupPolicy inherit action =
  runUnPrivilegedT userPolicy groupPolicy inherit action id

